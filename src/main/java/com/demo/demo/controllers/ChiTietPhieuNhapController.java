package com.demo.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.demo.models.ChiTietPhieuNhapKho;
import com.demo.demo.models.PhieuNhapKho;
import com.demo.demo.repositories.ChiTietPhieuNhapKhoRepository;

@Controller
@RequestMapping(path = "chi_tiet_phieu_nhap_kho")
public class ChiTietPhieuNhapController {
    @Autowired
    ChiTietPhieuNhapKhoRepository chiTietPhieuNhapKhoRep;
    // Return name of jsp file
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getAllVatTuHangHoa(ModelMap modelMap) {
        try {
            // Data sent to jsp modelMap
            // Sent data to jsp form controllers
            Iterable<ChiTietPhieuNhapKho> listChiTietPhieuNhapKho = chiTietPhieuNhapKhoRep.findAll();
            modelMap.addAttribute("listChiTietPhieuNhapKho", listChiTietPhieuNhapKho);
            return "DSChiTietPhieuNhapKho";
        } catch (Exception e) {
            return e.getMessage();
        }

    }
}
