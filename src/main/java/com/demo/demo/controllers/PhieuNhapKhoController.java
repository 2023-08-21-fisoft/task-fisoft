package com.demo.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.demo.models.PhieuNhapKho;
import com.demo.demo.repositories.PhieuNhapKhoRepository;

@Controller
@RequestMapping(path = "phieu-nhap-kho")
public class PhieuNhapKhoController {
    @Autowired
    PhieuNhapKhoRepository phieuNhapKhoRepository;

    // Return name of jsp file
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getAllVatTuHangHoa(ModelMap modelMap) {
        try {
            // Data sent to jsp modelMap
            // Sent data to jsp form controllers
            Iterable<PhieuNhapKho> listPhieuNhapKho = phieuNhapKhoRepository.findAll();
            modelMap.addAttribute("listPhieuNhapKho", listPhieuNhapKho);
            return "PhieuNhapKho";
        } catch (Exception e) {
            return e.getMessage();
        }

    }
}
