package com.demo.demo.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException.NotFound;

import com.demo.demo.models.VatTuHangHoa;
import com.demo.demo.repositories.VatTuHangHoaRepository;

@Controller
// http:localhost:8080/vat-tu-hang-hoa
@RequestMapping(path = "vat-tu-hang-hoa")

public class VatTuHangHoaController {
    @Autowired
    VatTuHangHoaRepository vatTuHangHoaRepository;

    // Return name of jsp file
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getAllVatTuHangHoa(ModelMap modelMap) {
        try {
            // Data sent to jsp modelMap
            // Sent data to jsp form controllers
            Iterable<VatTuHangHoa> vatTuHangHoa = vatTuHangHoaRepository.findAll();
            modelMap.addAttribute("vatTuHangHoa", vatTuHangHoa);
            return "VatTuHangHoa";
        } catch (Exception e) {
            return e.getMessage();
        }

    }

    @RequestMapping(value = "/ChiTietVatTuById/{id}", method = RequestMethod.GET)
    public String changeVatTuById(ModelMap modelMap, @PathVariable int id) {
        try{
            Optional<VatTuHangHoa> vatTuHangHoa = vatTuHangHoaRepository.findById(id);
            if(vatTuHangHoa.isPresent()) {
                modelMap.addAttribute("vatTuHangHoa", vatTuHangHoa.get());
            return "ChiTietNhapKho";
            }
            return "Not Found";
        }catch(Exception e){
            return e.getMessage();
        }
    }
    //  @RequestMapping(value = "/changeVatTuById/{id}", method = RequestMethod.POST)
    // public String updateVatTuById(ModelMap modelMap, @PathVariable int id, @ModelAttribute("vatTuHangHoa") VatTuHangHoa vatTuHangHoa ) {
    //     try{
    //         Optional<VatTuHangHoa> _vatTuHangHoa = vatTuHangHoaRepository.findById(id);
    //         if(_vatTuHangHoa.isPresent()) {
    //             Iterable<VatTuHangHoa> listVatTuHangHoa = vatTuHangHoaRepository.findAll();
    //             modelMap.addAttribute("vatTuHangHoa", listVatTuHangHoa);
    //         return "VatTuHangHoa";
    //         }
    //         return "Not Found";
    //     }catch(Exception e){
    //         return e.getMessage();
    //     }
    // }
}
