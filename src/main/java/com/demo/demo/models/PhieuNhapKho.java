package com.demo.demo.models;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "PhieuNhapKho")
public class PhieuNhapKho {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "SoPhieuNhap")
    private Integer soPhieuNhap;
    @Column(name = "NgayNhapKho")
    private Date ngayNhapKho;
    @Column(name = "MoTa")
    private String moTa;
    @Column(name = "DiaDiem")
    private String diaDiem;
    @OneToMany(mappedBy = "phieuNhapKho", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<ChiTietPhieuNhapKho> chiTietPhieuNhapKhoList;

    public PhieuNhapKho() {
    }

    public PhieuNhapKho(int id, Integer soPhieuNhap, Date ngayNhapKho, String moTa, String diaDiem) {
        this.id = id;
        this.soPhieuNhap = soPhieuNhap;
        this.ngayNhapKho = ngayNhapKho;
        this.moTa = moTa;
        this.diaDiem = diaDiem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getSoPhieuNhap() {
        return soPhieuNhap;
    }

    public void setSoPhieuNhap(Integer soPhieuNhap) {
        this.soPhieuNhap = soPhieuNhap;
    }

    public Date getNgayNhapKho() {
        return ngayNhapKho;
    }

    public void setNgayNhapKho(Date ngayNhapKho) {
        this.ngayNhapKho = ngayNhapKho;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getDiaDiem() {
        return diaDiem;
    }

    public void setDiaDiem(String diaDiem) {
        this.diaDiem = diaDiem;
    }

}
