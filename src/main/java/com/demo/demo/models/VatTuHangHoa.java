package com.demo.demo.models;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name = "vat_tu_hang_hoa")
public class VatTuHangHoa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "ten_vat_tu_hang_hoa")
    private String tenVatTuHangHoa;
    @Column(name = "mo_ta")
    private String moTa;
    @Column(name = "ten_quy_cach")
    private String tenQuyCach;
    @Column(name = "don_vi_tinh")
    private String donViTinh;
    @Column(name = "he_so")
    private BigDecimal heSo;
    @OneToMany(mappedBy = "vatTuHangHoa", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<ChiTietPhieuNhapKho> chiTietPhieuNhapKhoList;

    public VatTuHangHoa() {
    }

    public VatTuHangHoa(int id, String tenVatTuHangHoa, String moTa, String tenQuyCach, String donViTinh,
            BigDecimal heSo) {
        this.id = id;
        this.tenVatTuHangHoa = tenVatTuHangHoa;
        this.moTa = moTa;
        this.tenQuyCach = tenQuyCach;
        this.donViTinh = donViTinh;
        this.heSo = heSo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenVatTuHangHoa() {
        return tenVatTuHangHoa;
    }

    public void setTenVatTuHangHoa(String tenVatTuHangHoa) {
        this.tenVatTuHangHoa = tenVatTuHangHoa;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getTenQuyCach() {
        return tenQuyCach;
    }

    public void setTenQuyCach(String tenQuyCach) {
        this.tenQuyCach = tenQuyCach;
    }

    public String getDonViTinh() {
        return donViTinh;
    }

    public void setDonViTinh(String donViTinh) {
        this.donViTinh = donViTinh;
    }

    public BigDecimal getHeSo() {
        return heSo;
    }

    public void setHeSo(BigDecimal heSo) {
        this.heSo = heSo;
    }

}
