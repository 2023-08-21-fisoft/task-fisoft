package com.demo.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "chi_tiet_phieu_nhap_kho")
public class ChiTietPhieuNhapKho {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "so_luong")
    private Integer soLuong;
    @Column(name = "don_gia")
    private Long donGia;
    @ManyToOne
    @JoinColumn(name = "vat_tu_id")
    private VatTuHangHoa vatTuHangHoa;
    @ManyToOne
    @JoinColumn(name = "phieu_nhap_kho_id")
    private PhieuNhapKho phieuNhapKho;

    public ChiTietPhieuNhapKho() {
    }

    public ChiTietPhieuNhapKho(int id, Integer soLuong, Long donGia) {
        this.id = id;
        this.soLuong = soLuong;
        this.donGia = donGia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public VatTuHangHoa getVatTuHangHoa() {
        return vatTuHangHoa;
    }

    public void setVatTuHangHoa(VatTuHangHoa vatTuHangHoa) {
        this.vatTuHangHoa = vatTuHangHoa;
    }

    public PhieuNhapKho getPhieuNhapKho() {
        return phieuNhapKho;
    }

    public void setPhieuNhapKho(PhieuNhapKho phieuNhapKho) {
        this.phieuNhapKho = phieuNhapKho;
    }

    public Long getDonGia() {
        return donGia;
    }

    public void setDonGia(Long donGia) {
        this.donGia = donGia;
    }

}
