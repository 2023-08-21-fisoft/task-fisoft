package com.demo.demo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.demo.demo.models.VatTuHangHoa;
import java.util.List;


public interface VatTuHangHoaRepository extends CrudRepository<VatTuHangHoa, Integer>{
    
}
