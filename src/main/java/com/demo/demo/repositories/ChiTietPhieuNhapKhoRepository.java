package com.demo.demo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.demo.demo.models.ChiTietPhieuNhapKho;

public interface ChiTietPhieuNhapKhoRepository extends CrudRepository<ChiTietPhieuNhapKho, Integer> {
    
}
