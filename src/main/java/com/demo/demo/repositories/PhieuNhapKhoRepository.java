package com.demo.demo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.demo.demo.models.PhieuNhapKho;

public interface PhieuNhapKhoRepository extends CrudRepository<PhieuNhapKho, Integer>{
    
}
