<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>

    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
            integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
            crossorigin="anonymous" referrerpolicy="no-referrer" />
        <title>Vật tư hàng hóa</title>
    </head>

    <body>
        <h1 class="text-center mb-5">Bảng vật tư hàng hóa</h1>
        <div class="container">
            <a href=""><button class="btn btn-add btn-success mb-2">Thêm vật tư</button></a>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tên vật tư</th>
                        <th>Mô tả</th>
                        <th>Tên quy cách</th>
                        <th>Đơn vị tính</th>
                        <th>Hệ số</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="vatTuHangHoa" items="${vatTuHangHoa}">
                        <tr>
                            <td>${vatTuHangHoa.getId()}</td>
                            <td>${vatTuHangHoa.getTenVatTuHangHoa()}</td>
                            <td>${vatTuHangHoa.getMoTa()}</td>
                            <td>${vatTuHangHoa.getTenQuyCach()}</td>
                            <td>${vatTuHangHoa.getDonViTinh()}</td>
                            <td>${vatTuHangHoa.getHeSo()}</td>
                            <td>
                                <a href="vat-tu-hang-hoa/ChiTietVatTuById/${vatTuHangHoa.getId()}"><i class="fa-solid fa-pen-to-square icon-detail"></i></a>
                                
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        
    </body>

    </html>