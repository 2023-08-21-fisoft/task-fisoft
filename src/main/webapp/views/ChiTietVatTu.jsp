<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>

    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
            integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
            crossorigin="anonymous" referrerpolicy="no-referrer" />
        <title>Chi tiết vật tư</title>
    </head>
<body>
    <h1 class="text-center">Chi tiết vật tư</h1>
    <div class="container jumbotron">
        <form method="POST" modelAttribute = "vatTuHangHoa" action="/vat-tu-hang-hoa/changeVatTuById/${vatTuHangHoa.getId()}">
            <div class="row">
                <div class="col-5 form-group mr-2 ml-5">
                    <label for="">Tên</label>
                    <form:input type="text" 
                        class="form-control" 
                        path = "tenVatTuHangHoa" 
                        value ="${vatTuHangHoa.getTenVatTuHangHoa()}"
                        placeholder = "Nhập tên" >
                        ${vatTuHangHoa.getTenVatTuHangHoa()}
                    </form:input>
                </div>
                <div class="col-5 form-group">
                    <label for="">Mô tả</label>
                    <form:input type="text" 
                        class="form-control" 
                        path = "moTa" 
                        value="${vatTuHangHoa.getMoTa()}"
                        placeholder = "Nhập mô tả" >
                        ${vatTuHangHoa.getMoTa()}
                    </form:input>
                </div>
            </div>
            <div class="row">
                <div class="col-5 form-group mr-2 ml-5">
                    <label for="">Tên quy cách</label>
                    <form:input type="text" 
                        class="form-control" 
                        path = "tenQuyCach" 
                        value ="${vatTuHangHoa.getTenQuyCach()}"
                        placeholder = "Nhập tên quy cách" >
                        ${vatTuHangHoa.getTenQuyCach()}
                    </form:input>
                </div>
                <div class="col-5 form-group">
                    <label for="">Đơn vị tính</label>
                    <form:input type="text" 
                        class="form-control" 
                        path = "donViTinh" 
                        value="${vatTuHangHoa.getDonViTinh()}"
                        placeholder = "Nhập đơn vị tính" >
                        ${vatTuHangHoa.getDonViTinh()}
                    </form:input>
                </div>
            </div>
            <div class="row">
                <div class="col-5 form-group ml-5">
                    <label for="">Hệ số</label>
                    <form:input type="text" 
                        class="form-control" 
                        path = "heSo" 
                        value ="${vatTuHangHoa.getHeSo()}"
                        placeholder = "Nhập hệ số" >
                        ${vatTuHangHoa.getHeSo()}
                    </form:input>
                </div>
            </div>
        
        </form>
    </div>
    
</body>
</html>